import React from 'react';
import { render } from 'react-dom';
import App from './App.js';
import './css/bootstrap.min.css';

render(<App/>, document.getElementById('root'));