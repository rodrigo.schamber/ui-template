const api = "http://localhost:8081"
const headers = {
  Accept: "application/json",
  "Content-Type": "application/json"
}

export async function deleteLiked(id){
    
    try {
    
        const content = await fetch(`${api}/liked/${id}`, {
            method:'DELETE',
            headers
        });
        
        return content.json()
    
    } catch (error) {
    
        console.log(error)
    
    }
};

export async function getLiked(id){

    try {
    
        const content = await fetch(`${api}/liked/${id}`, {
            method:'GET',
            headers
        });
        
        return content.json()
    
    } catch (error) {
    
        console.log(error)
    
    }

};

export async function postLiked(id, liked, comment){

    try {
    
        const content = await fetch(`${api}/liked/`, {
            method:'POST',
            headers,
            body: JSON.stringify(
                {
                    id: id,
                    liked: liked,
                    comment: comment
                }
            )
        });
        
        return content.json()
    
    } catch (error) {
    
        console.log(error)
    
    }

};

export async function putLiked(id, liked, comment){

    try {
    
        const content = await fetch(`${api}/liked/${id}`, {
            method:'PUT',
            headers,
            body: JSON.stringify(
                {
                    id: id,
                    liked: liked,
                    comment: comment
                }
            )
        });
        
        return content.json()
    
    } catch (error) {
    
        console.log(error)
    
    }

};