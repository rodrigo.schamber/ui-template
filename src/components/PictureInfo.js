export default class PictureInfo {
    constructor(id, liked, comment){
        this.id = id;
        this.liked = liked;
        this.comment = comment;
    }
}