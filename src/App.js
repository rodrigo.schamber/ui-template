import React, {useEffect, useState} from 'react';
import {Button, Card, Col, Container, Form, FormControl, InputGroup, Row} from 'react-bootstrap';
import moment from 'moment';
import NasaPicture from './components/NasaPicture';
import PictureInfo from './components/PictureInfo';
import getPicture from './gateway/getPicture';
import DatePicker from 'react-datepicker';
import {deleteLiked, getLiked, postLiked, putLiked} from './gateway/userAPI.js';
import "./css/react-datepicker.css";

export default function App() {
  const[comment, setComment] = useState(new String());
  const [picture, setPicture] = useState(new NasaPicture());
  const [pictureInfo, setPictureInfo] = useState(new PictureInfo());
  const [startDate, setStartDate] = useState(new Date());
  
  function formatDate(dateToFormat){
    
    return moment(dateToFormat).format().slice(0,10)
  
  };

  function handleCommentChange(event){

    setComment(event);

  };

  function handleDateChange(date){
  
    loadPicture(date);
    loadPictureInfo(date);
    setStartDate(date);
  
  };

  function handleDeleteComment(id){
    
    deleteLiked(id).then(() => {
    
      setPictureInfo({comment: "Please insert a comment."})
    
    });
    
  };

  function handleEditComment(id, liked, comment){
    
    putLiked(id, liked, comment).then(() => {
    
      setPictureInfo({comment: comment})
    
    });

  };

  function handleSaveComment(id, liked, comment){
    
    postLiked(id,  liked, comment).then(() => {
    
      setPictureInfo({comment: comment})
    
    });

  };

  function loadPicture(date){
    
    getPicture(formatDate(date)).then((item) => {
      
      setPicture(item);
    
    });
  
  };

  function loadPictureInfo(date){
    
    getLiked(formatDate(date)).then((item) => {

      setPictureInfo(item);
    
    });
  
  };
  
  useEffect(() => {
    
    loadPicture(startDate)
    loadPictureInfo(startDate);
  
  }, []);
  
  return (
    <Container fluid>
      <Row className="justify-content-lg-center">
        <Col lg={2}>
          <Form>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label><h6>Choose a date:</h6></Form.Label>
                <DatePicker
                  selected={startDate}
                  onChange={(date) => {
                          
                    handleDateChange(date);
                        
                  }}
                />
              </Form.Group>
            </Form.Row>
          </Form>
        </Col>
        <Col lg={6}>
          <Card>
            <Card.Header>
              <Card.Title><h5>{`${picture.title}`}</h5></Card.Title>
            </Card.Header>
            <Card.Img variant="top" src={picture.url}/>
            <Card.Body>
              <Card.Text>
                {picture.explanation}
              </Card.Text>
              <Card.Text style={{color: "red"}}>
                <small>
                  <i>
                    {(pictureInfo.comment !== undefined) ? `${pictureInfo.comment}` : `Please insert a comment.`}
                  </i>
                </small>
              </Card.Text>       
            </Card.Body>
          </Card>
          <InputGroup className="mb-3">
            <FormControl
              id="ui-template-comment-input"
              onChange={(event) => handleCommentChange(event.target.value)}
              placeholder={`Write a comment`}
              style={{
                margin: '1vh',
                marginLeft: '0vh',
                marginRight: '0vh'
              }}
              value={comment}
            />
            <InputGroup.Append>
              <Button
                onClick={() => {
                
                  if (document.getElementById("ui-template-comment-input").value.length !== 0) {
                  
                    handleSaveComment(formatDate(startDate), false, comment);
                  
                  } else {
                  
                    alert("Please insert a comment");
                  
                  };
                    
                }}
                style={{
                  margin: '1vh',
                  marginRight: '0vh'
                }}
                variant="outline-primary"
              >
                Save
              </Button>
              <Button
                onClick={() => {
                  
                  if (document.getElementById("ui-template-comment-input").value.length !== 0) {
                  
                    handleEditComment(formatDate(startDate), false, comment);
                  
                  } else {
                  
                    alert("Please insert a comment");
                  
                  };
                      
                }}
                style={{
                  margin: '1vh',
                  marginRight: '0vh'
                }}
                variant="outline-secondary"
              >
                Edit
              </Button>
              <Button
                onClick={() => {
                          
                  handleDeleteComment(formatDate(startDate));
                  setComment("");
                      
                }}
                style={{
                  margin: '1vh',
                  marginRight: '0vh'
                }}
                variant="outline-danger"
              >
                Delete
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </Col>
      </Row>
    </Container>
  );
};