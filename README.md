# UI-TEMPLATE
## _UI Weeks 1 and 2 Practice Assignment_

This code has the purpose to demonstrate some UI code tools.

## Installation

ui-template requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and start the server.

```sh
cd ui-template
yarn server
```

Open another tab and:

```sh
cd ui-template
yarn install
yarn start
```

## License

MIT